using System;
using UnityEngine;

namespace HairChallenge.Core.Data
{
    [CreateAssetMenu(fileName = "CurrenciesData", menuName = "Data/CurrenciesData")]
    public class CurrenciesData : ScriptableObject
    {
        [SerializeField] private Currency[] _currencies;

        public Currency GetCurrency(string id)
        {
            foreach (var currency in _currencies)
            {
                if (currency.Id.Equals(id))
                {
                    return currency;
                }
            }
            return null;
        }
    }

    [Serializable]
    public class Currency
    {
        [SerializeField] private string _currency;
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;

        public string Id => _currency;
        public string Name => _name;
        public Sprite Icon => _icon;
    }
}
