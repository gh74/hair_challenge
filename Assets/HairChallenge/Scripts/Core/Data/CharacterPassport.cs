﻿using System;
using HairChallenge.Core.Controllers.Player;
using UnityEngine;

namespace HairChallenge.Core.Data
{
    [Serializable]
    public class CharacterPassport
    {
        [SerializeField] private string _id;
        [SerializeField] private string _name;
        [SerializeField] private PlayerUnit _playerUnit;
        [SerializeField] private GameResource _unlockCost;
        [SerializeField] private WearItem[] _items;

        public string Id => _id;
        public string Name => _name;
        public PlayerUnit Prefab => _playerUnit;
        public GameResource Cost => _unlockCost;
        public WearItem[] WearItems => _items;
    }
}