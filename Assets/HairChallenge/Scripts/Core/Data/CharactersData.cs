using UnityEngine;

namespace HairChallenge.Core.Data
{
    [CreateAssetMenu(fileName = "CharacterData", menuName = "Data/CharacterData")]
    public class CharactersData : ScriptableObject
    {
        [SerializeField] public CharacterPassport[] characters;
    }
}
