using System.Collections;
using System.Collections.Generic;
using HairChallenge.Core.Data;
using UnityEngine;

public class SaveData
{
    public List<GameResource> currencies;
    public List<string> characters;
    public List<string> wearableItems;
    public string currentCharacter;

    public SaveData()
    {
        currencies = new List<GameResource>();
        characters = new List<string>();
        wearableItems = new List<string>();
    }
}
