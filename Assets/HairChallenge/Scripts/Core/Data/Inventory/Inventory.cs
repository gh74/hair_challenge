using System.Collections.Generic;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;

namespace HairChallenge.Core.Data.Inventory
{
    public sealed class Inventory : IService
    {
        private const string SaveDataKey = "SAVE_DATA";

        private List<GameResource> _currencies;
        private List<string> _characters;
        private List<string> _wearableItems;
        private string _currentCharacter;

        public void Load()
        {
            //Default Inventory. TODO Define in Settings
            if (!PlayerPrefs.HasKey(SaveDataKey))
            {
                _currencies = new List<GameResource>();
                _characters = new List<string>();
                _wearableItems = new List<string>();
                _currentCharacter = "sandy";
                Save();
            }
            else
            {
                var json = PlayerPrefs.GetString(SaveDataKey);
                var saveData = JsonUtility.FromJson<SaveData>(json);
            }
        }

        public void Save()
        {
            var saveData = new SaveData()
            {
                currencies = _currencies,
                characters = _characters,
                wearableItems = _wearableItems,
                currentCharacter = _currentCharacter
            };

            var json = JsonUtility.ToJson(saveData);
            PlayerPrefs.SetString(SaveDataKey, json);
            PlayerPrefs.Save();
        }
    }
}
