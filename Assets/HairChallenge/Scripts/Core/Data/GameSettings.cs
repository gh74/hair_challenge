using System;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;

namespace HairChallenge.Core.Data
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Data/GameSettings")]
    public class GameSettings : ScriptableObject, IService
    {
        [SerializeField] private int _targetFps=60;
        [SerializeField] private PlayerMovePhysics _playerMovePhysics;
        [SerializeField] private CameraFollowSettings _cameraFollowSettings;

        public int TargetFps => _targetFps;
        public PlayerMovePhysics PlayerPhysics => _playerMovePhysics;
        public CameraFollowSettings CameraSettings => _cameraFollowSettings;
        
        [Serializable]
        public class PlayerMovePhysics
        {
            [SerializeField] public float forwardVelocity=5;
            [SerializeField] public float strafeVelocity=3;
        }
        
        [Serializable]
        public class CameraFollowSettings
        {
            [SerializeField] public Vector3 offset;
            [SerializeField] public float smoothness;
            [SerializeField] public Vector3 rotation;
        }
    }
}
