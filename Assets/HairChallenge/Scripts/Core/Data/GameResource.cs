﻿using System;
using UnityEngine;

namespace HairChallenge.Core.Data
{
    [Serializable]
    public class GameResource
    {
        [SerializeField] public string _currency;
        [SerializeField] public int _value;

        public string Currency => _currency;
        public int Value => _value;

        public GameResource(string currency, int value)
        {
            _currency = currency;
            _value = value;
        }
        
    }
}