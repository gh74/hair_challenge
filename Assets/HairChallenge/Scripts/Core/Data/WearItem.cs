﻿using System;
using UnityEngine;

namespace HairChallenge.Core.Data
{
    [Serializable]
    public class WearItem
    {
        [SerializeField] private GameObject _item;
        [SerializeField] private GameResource _cost;
        [SerializeField] private ItemType _itemType;
        
        public enum ItemType
        {
            Hat,
            Top,
            Pants,
            Boots,
            Gloves
        }
    }
}