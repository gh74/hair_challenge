using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace HairChallenge.Core.Gameplay.InteractableSystem
{
    public class Collectable : Interactor
    {
        [SerializeField] private ParticleSystem _particleEffect;
        [SerializeField] private Transform _model;
        
        public override void Interact(Action<string, string, float> callback)
        {
            StartCoroutine(Collect(callback));
            GetComponent<Collider>().enabled = false;
        }

        private IEnumerator Collect(Action<string, string, float> callback)
        {
            yield return null;
            callback?.Invoke(_id, _extraOperation, _extraValue);
            
            if (_particleEffect != null)
            {
                _particleEffect.Emit(50);
            }
            _model.DORotate(Vector3.up * 720, 0.5f, RotateMode.FastBeyond360);
            _model.DOScale(Vector3.zero, 0.5f);
            _model.DOLocalMove(Vector3.up * 3f, 0.5f);

        }
    }
}
