using System;
using UnityEngine;

namespace HairChallenge.Core.Gameplay.InteractableSystem
{
    [RequireComponent(typeof(Collider))]
    public class Interactor : MonoBehaviour
    {
        [SerializeField] protected string _id;
        [SerializeField] protected string _extraOperation;
        [SerializeField] protected float _extraValue;

        public virtual void Interact(Action<string, string, float> callback)
        {
            callback?.Invoke(_id, _extraOperation, _extraValue);
            gameObject.SetActive(false);
            GetComponent<Collider>().enabled = false;
        }
    }
}
