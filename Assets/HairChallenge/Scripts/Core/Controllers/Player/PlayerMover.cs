using System;
using HairChallenge.Core.Data;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;

namespace HairChallenge.Core.Controllers.Player
{
    public class PlayerMover : MonoBehaviour, IMover
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private HairController _hair;
        
        private Rigidbody _rigidbody;
        private Vector3 _velocity = Vector3.zero;
        private GameSettings.PlayerMovePhysics _playerMovePhysics;

        private bool _isMoving;
        private bool _isEnabled = true;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _playerMovePhysics = ServiceLocator.Resolve<GameSettings>().PlayerPhysics;
            _animator.Play("Idle");
        }

        public void MoveForward()
        {
            if (!_isEnabled)
            {
                return;
            }
            transform.position += Vector3.forward * _playerMovePhysics.forwardVelocity;
            _hair.SetVelocity(_playerMovePhysics.forwardVelocity);
            if (!_isMoving)
            {
                _isMoving = true;
                _animator.Play("Run");
            }
        }

        public void Stop()
        {
            if (_isMoving)
            {
                _isMoving = false;
                _animator.Play("Idle");
            }
        }

        public void MoveLeft(float starafeSpeed)
        {
            if (!_isEnabled)
            {
                return;
            }

            transform.position += Vector3.left * _playerMovePhysics.strafeVelocity * starafeSpeed;
            _hair.AddInertia(_playerMovePhysics.strafeVelocity * starafeSpeed * 0.1f);
            
        }

        public void MoveRight(float starafeSpeed)
        {
            if (!_isEnabled)
            {
                return;
            }
            transform.position += Vector3.right * _playerMovePhysics.strafeVelocity * starafeSpeed;
            _hair.AddInertia(_playerMovePhysics.strafeVelocity * -1 * starafeSpeed * 0.1f);
        }

        public void AnimateFinish()
        {
            _animator.Play("Finish");
        }

        public void Deactivate()
        {
            _isEnabled = false;
        }
    }
}
