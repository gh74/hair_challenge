namespace HairChallenge.Core.Controllers.Player
{
    public interface IMover
    {
        void MoveForward();
        void MoveLeft(float starfeSpeed);
        void MoveRight(float starfeSpeed);
        void Stop();
        void AnimateFinish();
        void Deactivate();
    }
}
