﻿using System;
using HairChallenge.Core.Controllers.PlayerInput;
using HairChallenge.Core.Gameplay.InteractableSystem;
using HairChallenge.Patterns.ServiceLocator;
using HairChallenge.UI;
using HairChallenge.UI.Screens.ProgressDisplay;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HairChallenge.Core.Controllers.Player
{
    public class PlayerUnit : MonoBehaviour, IService
    {
        private IMover _mover;
        private IInput _input;
        [SerializeField] private HairController _hair;

        private float _length = 0.5f;
        private float _hairTime;
        private int _comboCounter;

        private ProgressDisplayController _progressDisplay;

        private void Awake()
        {
            ServiceLocator.RegisterService<PlayerUnit>(this);
        }

        private void Start()
        {
            _mover = GetComponent<IMover>();
            _input = ServiceLocator.Resolve<IInput>();
            _input.RegisterCallbacks(_mover.MoveForward, _mover.Stop, _mover.MoveLeft, _mover.MoveRight);
            _progressDisplay = ServiceLocator.Resolve<UIManager>().GetScreen<ProgressDisplayController>();
            _hair.SetLength(_length);
        }

        internal void AnimateFinish()
        {
            _mover.AnimateFinish();
        }

        internal void Deactivate()
        {
            _mover.Deactivate();
        }

        private void HandleInteraction(string id, string extraOperation, float extraValue)
        {
            switch (id)
            {
                case "hair":
                    Increase(0.5f);
                    break;
                case "cutter":
                    Increase(_length/2f);
                    break;
                case "finish":
                    ServiceLocator.Resolve<Gameplay.GameplayController>().FinishGame();
                    break;
                case "gate":
                    switch (extraOperation)
                    {
                        case "div":
                            Decrease(_length - _length/extraValue);
                            break;
                        case "subtract":
                            Decrease(extraValue);
                            break;
                        case "add":
                            Increase(extraValue);
                            break;
                        case "mult":
                            Increase(_length * extraValue - _length);
                            break;
                    }
                    break;
            }
        }

        private void Decrease(float val)
        {
            float prevLen = _length;
            _length =  Mathf.Clamp(_length-val,0.5f,float.MaxValue);
            _hair.SetLength(_length);
            _progressDisplay.DisplayProgress((int)((_length - prevLen)*100),
                (int)(_length*100), 0, false);
            _comboCounter = 0;
        }

        private void Increase(float val)
        {
            _length += 0.5f;
            _hair.SetLength(_length);
            if (Time.time - _hairTime < 0.5f)
            {
                _comboCounter++;
            }
            else
            {
                _comboCounter = 0;
            }

            _hairTime = Time.time;
            _progressDisplay.DisplayProgress(50,(int)(_length*100), 0, _comboCounter>2);
            if (_comboCounter > 2)
            {
                _comboCounter = 0;
            }
        }

        private void OnTriggerStay(Collider other)
        {
            Interactor interactor = other.GetComponent<Interactor>();
            if (interactor != null)
            {
                interactor.Interact(HandleInteraction);
            }
        }
    }
}