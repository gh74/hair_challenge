using System;
using System.Collections;
using System.Collections.Generic;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;
using Random = UnityEngine.Random;

public class HairController : MonoBehaviour
{
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private ParticleSystem _sparkles;

    private List<Vector3> _points = new List<Vector3>();

    private float _length = 2f;
    
    private int _segmentsPerUnit = 16;
    private float _inertiaClamp = 3f;
    private float _baseHairVelocity = 2f;
    private float _minHairLengthWithInertia = 1f;
    private float _waveRatio = 3f;
    private int _unwavedSegments = 3;

    private int _segments;
    private float _segmentLength;
    private float _wave;
    private float _waveVal;
    private float _inertia;
    private float _inertialOffset;
    private float _velocity;

    private void Start()
    {
        _segmentLength = 1f / _segmentsPerUnit;
    }

    public void SetLength(float length)
    {
        _length = length;
    }

    public void AddInertia(float val)
    {
        _inertia = Mathf.Clamp(_inertia + val,-1f*_inertiaClamp,_inertiaClamp);
    }

    public void SetVelocity(float value)
    {
        _velocity = _baseHairVelocity + value*10;
    }
    
    void FixedUpdate()
    {
        _segments = (int)(_length * _segmentsPerUnit);
        _points.Clear();
        
        for (int i = 0; i < _segments; i++)
        {
            _wave =  (float) i / _waveRatio;
            _waveVal = i > _unwavedSegments ? (float)i / _segments : 0;
            _inertialOffset = _length>_minHairLengthWithInertia ? 0.003f * _velocity * i * i * _inertia / _length : 0;
            
            _points.Add(new Vector3(
                Mathf.Sin(_wave + Time.time*_velocity)*_waveVal*0.25f + _inertialOffset,
                -1 * _waveVal*1f+ 
                (_velocity>2.1f && _length>2f ? Mathf.Sin(_wave + Time.time*_velocity)*_waveVal*0.25f : 0),
                -1 * i * _segmentLength ));
        }
        
        _lineRenderer.positionCount = _segments;
        _lineRenderer.SetPositions(_points.ToArray());
        
        _inertia = Mathf.Lerp(_inertia, 0, 0.1f);
        _velocity = Mathf.Lerp(_velocity, _baseHairVelocity, 1f);//No interpolation (Looks bad)

        if (Time.frameCount % 3 == 0)
        {
            _sparkles.transform.localPosition =
                _points[Random.Range(0, _segments)] + new Vector3(Random.Range(-0.1f, 0.1f), 0.1f, 0f);
        }
        
    }
}
