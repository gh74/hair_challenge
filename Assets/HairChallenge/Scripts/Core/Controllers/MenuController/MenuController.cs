using System.Collections;
using System.Collections.Generic;
using HairChallenge.Patterns.ServiceLocator;
using HairChallenge.UI;
using HairChallenge.UI.Screens.PlayMenuBar;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    private UIManager _uiManager;
    
    void Start()
    {
        _uiManager = ServiceLocator.Resolve<UIManager>();
        _uiManager.OpenScreen<PlayMenuBarController>();
    }

}
