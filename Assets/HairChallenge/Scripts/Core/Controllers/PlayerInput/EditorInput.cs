using System;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;

namespace HairChallenge.Core.Controllers.PlayerInput
{
    public class EditorInput : BaseInput
    {

        private void Start()
        {
            strafeSpeed = 1f;
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.W))
            {
                _forwardPressed = true;
            }
            else
            {
                _forwardPressed = false;
            }
            
            if (Input.GetKey(KeyCode.A))
            {
                _leftPressed = true;
            }
            else
            {
                _leftPressed = false;
            }
            
            if (Input.GetKey(KeyCode.D))
            {
                _rightPressed = true;
            }
            else
            {
                _rightPressed = false;
            }

            if (_leftPressed && _rightPressed)
            {
                _leftPressed = false;
                _rightPressed = false;
            }
        }
    }
}
