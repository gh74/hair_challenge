using System;
using UnityEngine;

namespace HairChallenge.Core.Controllers.PlayerInput
{
    public abstract class BaseInput : MonoBehaviour, IInput
    {
        private Action _onMoveForward;
        private Action _onStop;
        private Action<float> _onMoveLeft;
        private Action<float> _onMoveRight;

        protected bool _leftPressed;
        protected bool _rightPressed;
        protected bool _forwardPressed;
        protected float strafeSpeed;

        private bool isStoped = false;

        private void FixedUpdate()
        {
            if (_forwardPressed)
            {
                isStoped = false;
                _onMoveForward?.Invoke();
            }
            else
            {
                if (!isStoped)
                {
                    isStoped = true;
                    _onStop?.Invoke();
                }
            }

            if (_leftPressed)
            {
                _onMoveLeft?.Invoke(strafeSpeed);
            }

            if (_rightPressed)
            {
                _onMoveRight?.Invoke(strafeSpeed);
            }
        }

        public void RegisterCallbacks(Action OnMoveForward, Action OnStop, Action<float> OnMoveLeft, Action<float> OnMoveRight)
        {
            _onMoveForward = OnMoveForward;
            _onStop = OnStop;
            _onMoveLeft = OnMoveLeft;
            _onMoveRight = OnMoveRight;
        }
    }
}
