using System;
using HairChallenge.Patterns.ServiceLocator;

namespace HairChallenge.Core.Controllers.PlayerInput
{
    public interface IInput : IService
    {
        void RegisterCallbacks(Action OnMoveForward, Action OnStop, Action<float> OnMoveLeft, Action<float> OnMoveRight);
    }
}
