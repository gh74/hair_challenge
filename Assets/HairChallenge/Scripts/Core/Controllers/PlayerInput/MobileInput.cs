using System.Collections;
using System.Collections.Generic;
using HairChallenge.Core.Controllers.PlayerInput;
using UnityEngine;

public class MobileInput : BaseInput
{

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _forwardPressed = true;
            if (Input.mousePosition.x < ((float) Screen.width) * 0.5f)
            {
                strafeSpeed = (((float)Screen.width) * 0.5f - Input.mousePosition.x) / ((float)Screen.width) * 0.5f;
                _leftPressed = true;
            }
            else
            {
                _leftPressed = false;
            }
            if (Input.mousePosition.x > ((float) Screen.width) * 0.5f)
            {
                strafeSpeed = (Input.mousePosition.x - ((float)Screen.width) * 0.5f) / ((float)Screen.width) * 0.5f;
                _rightPressed = true;
            }
            else
            {
                _rightPressed = false;
            }
        }
        else
        {
            _forwardPressed = false;
            _leftPressed = false;
            _rightPressed = false;
        }
    }
}
