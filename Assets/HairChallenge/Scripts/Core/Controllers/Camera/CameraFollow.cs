using HairChallenge.Core.Controllers.Player;
using HairChallenge.Core.Data;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;

namespace HairChallenge.Core.Controllers.Camera
{
    public class CameraFollow : MonoBehaviour
    {
        private Transform _target;
        private GameSettings.CameraFollowSettings _cameraFollowSettings;
        void Start()
        {
            _target = ServiceLocator.Resolve<PlayerUnit>().transform;
            _cameraFollowSettings = ServiceLocator.Resolve<GameSettings>().CameraSettings;
            transform.SetPositionAndRotation(_target.position + _cameraFollowSettings.offset, Quaternion.identity);
        }
        
        void FixedUpdate()
        {
            transform.SetPositionAndRotation( Vector3.Lerp(transform.position, _target.position + _cameraFollowSettings.offset,
                Time.deltaTime * _cameraFollowSettings.smoothness),
            Quaternion.Euler(_cameraFollowSettings.rotation));
            
        }
    }
}
