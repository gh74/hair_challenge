using HairChallenge.Core.Controllers.Player;
using HairChallenge.Patterns.ServiceLocator;
using HairChallenge.UI;
using HairChallenge.UI.Screens.GameplayScreen;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HairChallenge.Core.Controllers.Gameplay
{
    public class GameplayController : MonoBehaviour, IService
    {
        private GameplayScreenController _gameplayScreen;

        private void Awake()
        {
            ServiceLocator.RegisterService<GameplayController>(this);
        }

        void Start()
        {
            _gameplayScreen = ServiceLocator.Resolve<UIManager>().GetScreen<GameplayScreenController>();
            _gameplayScreen.Open();
        }


        public void FinishGame()
        {
            ServiceLocator.Resolve<UIManager>().GetScreen<GameplayScreenController>().Close();
            StartCoroutine(PlayFinish());
        }

        private IEnumerator PlayFinish()
        {
            var player = ServiceLocator.Resolve<PlayerUnit>();
            player.Deactivate();
            player.AnimateFinish();
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("Menu");
        }
    }
}
