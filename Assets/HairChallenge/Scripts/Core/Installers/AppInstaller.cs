using HairChallenge.Core.Controllers.PlayerInput;
using HairChallenge.Core.Data;
using HairChallenge.Core.Data.Inventory;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HairChallenge.Core.Installers
{
    public class AppInstaller : MonoBehaviour
    {
        [SerializeField] private GameSettings _gameSettings;

        private static AppInstaller _instance;
    
        private void Awake()
        {
            if (_instance == null && SceneManager.GetActiveScene().buildIndex!=0)
            {
                SceneManager.LoadScene(0);
                return;
            }
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
                InstallGame();
                if (SceneManager.GetActiveScene().buildIndex==0)
                {
                    SceneManager.LoadScene(1);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void InstallGame()
        {
            ServiceLocator.RegisterService<GameSettings>(_gameSettings);
            Application.targetFrameRate = _gameSettings.TargetFps;

            var inventory = new Inventory();
            inventory.Load();
            ServiceLocator.RegisterService(inventory);

            IInput input = new GameObject("Input").AddComponent<EditorInput>();
            ServiceLocator.RegisterService<IInput>(input);
        }
    }
}
