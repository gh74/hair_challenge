using System.Collections;
using System.Collections.Generic;
using HairChallenge.Patterns.ServiceLocator;
using HairChallenge.UI;
using HairChallenge.UI.Screens.GameplayScreen;
using HairChallenge.UI.Screens.PlayMenuBar;
using HairChallenge.UI.Screens.ProgressDisplay;
using HairChallenge.UI.UIFramework;
using UnityEngine;

public class UIInstaller : MonoBehaviour
{
    [SerializeField] private ScreenView _playMenuBarView;
    [SerializeField] private ScreenView _progressDisplay;
    [SerializeField] private ScreenView _gameplayView;
    void Start()
    {
        var uiManager = ServiceLocator.Resolve<UIManager>();
        
        uiManager.RegisterScreen<PlayMenuBarController>(new PlayMenuBarController(), _playMenuBarView);
        uiManager.RegisterScreen<ProgressDisplayController>(new ProgressDisplayController(), _progressDisplay);
        uiManager.RegisterScreen<GameplayScreenController>(new GameplayScreenController(), _gameplayView);
    }
    
}
