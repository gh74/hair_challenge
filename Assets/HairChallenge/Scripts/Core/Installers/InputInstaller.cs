using HairChallenge.Core.Controllers.PlayerInput;
using HairChallenge.Patterns.ServiceLocator;
using UnityEngine;

namespace HairChallenge.Core.Installers
{
    public class InputInstaller : MonoBehaviour
    {
        private void Awake()
        {
            var input = gameObject.AddComponent<MobileInput>();
            ServiceLocator.RegisterService<IInput>(input);
        }
    }
}
