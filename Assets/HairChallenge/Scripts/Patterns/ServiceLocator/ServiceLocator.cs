using System;
using System.Collections.Generic;

namespace HairChallenge.Patterns.ServiceLocator
{
    public class ServiceLocator
    {

        private static Dictionary<Type, IService> _services = new Dictionary<Type, IService>();

        public static void RegisterService<T>(T service) where T : IService
        {
            if (_services.ContainsKey(typeof(T)))
            {
                _services[typeof(T)] = service;
            }
            else
            {
                _services.Add(typeof(T),service);
            }
        }

        public static T Resolve<T>() where T : IService
        {
            if (_services.ContainsKey(typeof(T)))
            {
                return (T)_services[typeof(T)];
            }
            return default;
        }
    }
}
