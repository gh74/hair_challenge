using HairChallenge.UI.UIFramework;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HairChallenge.UI.Screens.PlayMenuBar
{
    public class PlayMenuBarController : ScreenController
    {
        public override void OnInit()
        {
            ((PlayMenuBarView)_view).RegisterCallbacks(OnPlayButtonPressed);
        }

        private void OnPlayButtonPressed()
        {
            Close();
            SceneManager.LoadScene("Game");
        }
    }
}
