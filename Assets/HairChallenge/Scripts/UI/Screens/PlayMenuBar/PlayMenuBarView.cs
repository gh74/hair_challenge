using System;
using HairChallenge.UI.UIFramework;
using UnityEngine;
using UnityEngine.UI;

namespace HairChallenge.UI.Screens.PlayMenuBar
{
    public class PlayMenuBarView : ScreenView
    {
        [SerializeField] private Button _playButton;
        
        public void RegisterCallbacks(Action onPlayButtonPressed)
        {
            _playButton.onClick.AddListener(onPlayButtonPressed.Invoke);
        }
    }
}
