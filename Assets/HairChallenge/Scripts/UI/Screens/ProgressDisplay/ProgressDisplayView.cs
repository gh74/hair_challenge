using System;
using DG.Tweening;
using HairChallenge.UI.UIFramework;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace HairChallenge.UI.Screens.ProgressDisplay
{
    public class ProgressDisplayView : ScreenView
    {
        private const float _FadeTime = 1.0f;

        [SerializeField] private Transform _labelPool;
        [SerializeField] private Transform _labelsInUse;
        [SerializeField] private Color[] _positiveColors;
        [SerializeField] private Color[] _negativeColors;
        [SerializeField] private string[] combos;
        
        public void DisplayProgress(int value, int totalProgress, int targetProgress, bool combo)
        {
            var label = PoolLabel();

            if (label == null)
            {
                return;
            }
            var color = value > 0
                ? _positiveColors[Random.Range(0, _positiveColors.Length)]
                : _negativeColors[Random.Range(0, _negativeColors.Length)];
            label.color = color*0.85f;
            label.text = $@"{(value>0 ? "+" : "")}{value}{(combo ? " " + combos[Random.Range(0,combos.Length)] : "")}";
            label.DOColor(color,1f).OnComplete(() =>
                {
                    label.DOFade(0f, _FadeTime).SetEase(Ease.OutBack).OnComplete(() => { PushLabel(label); });
                });
            
        }

        private TextMeshProUGUI PoolLabel()
        {
            var labels = _labelPool.GetComponentsInChildren<TextMeshProUGUI>();
            if (labels != null && labels.Length > 0)
            {
                var label = labels[Random.Range(0, labels.Length)];
                label.transform.parent = _labelsInUse;
                return label;
            }
            return null;
        }

        private void PushLabel(TextMeshProUGUI label)
        {
            label.transform.parent = _labelPool;
        }
    }
}
