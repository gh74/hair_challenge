using HairChallenge.UI.UIFramework;

namespace HairChallenge.UI.Screens.ProgressDisplay
{
    public class ProgressDisplayController : ScreenController
    {
        public void DisplayProgress(int value, int totalProgress, int targetProgress, bool combo)
        {
            ((ProgressDisplayView)_view).DisplayProgress(value,totalProgress,targetProgress, combo);
        }
    }
}
