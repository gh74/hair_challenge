using System;
using HairChallenge.Patterns.ServiceLocator;
using HairChallenge.UI.UIFramework;
using UnityEngine;

namespace HairChallenge.UI
{
    public class UIManager : UIFrameworkManager, IService
    {
        private void Awake()
        {
            ServiceLocator.RegisterService<UIManager>(this);
            DontDestroyOnLoad(gameObject);
        }
    }
}
