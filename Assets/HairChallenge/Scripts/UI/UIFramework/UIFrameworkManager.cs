using System;
using System.Collections.Generic;
using UnityEngine;

namespace HairChallenge.UI.UIFramework
{
    public abstract class UIFrameworkManager : MonoBehaviour
    {
        private Dictionary<Type, ScreenController> _screens = new Dictionary<Type, ScreenController>();

        public void RegisterScreen<T>(ScreenController controller, ScreenView view, ScreenModel model = null) where T : ScreenController
        {
            controller.Init(view, model);
            _screens.Add(typeof(T),controller);
        }

        public T GetScreen<T>() where T : ScreenController
        {
            if (_screens.ContainsKey(typeof(T)))
            {
                return (T)_screens[typeof(T)];
            }
            return default;
        }

        public void OpenScreen<T>() where T : ScreenController
        {
            var screen = GetScreen<T>();
            if (screen != default)
            {
                screen.Open();
            }
        }

        public void CloseScreen<T>() where T : ScreenController
        {
            var screen = GetScreen<T>();
            if (screen != default)
            {
                screen.Close();
            }
        }
    
    }
}
