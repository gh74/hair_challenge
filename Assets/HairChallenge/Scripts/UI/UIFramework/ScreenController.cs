namespace HairChallenge.UI.UIFramework
{
    public abstract class ScreenController
    {
        protected ScreenView _view;
        protected ScreenModel _model;
        
        public void Init(ScreenView view, ScreenModel model)
        {
            _view = view;
            _model = model;
            OnInit();
            _view.OnInit();
        }

        public virtual void OnInit()
        {
            
        }

        public virtual void Open()
        {
            _view.Open();
        }
        
        public virtual void Close()
        {
            _view.Close();
        }
    }
}
