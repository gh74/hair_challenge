using System;
using UnityEngine;

namespace HairChallenge.UI.UIFramework
{
    public abstract class ScreenView : MonoBehaviour
    {
        private ScreenModel _model;
        
        public void Init(ScreenModel model)
        {
            _model = model;
            OnInit();
        }
        public virtual void OnInit()
        {
            
        }

        public virtual void Open()
        {
            gameObject.SetActive(true);
        }

        public virtual void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
